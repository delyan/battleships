#ifndef BATTLESHIPS_USER_H
#define BATTLESHIPS_USER_H

#include "util.h"

void makePlayerTurn(int readFd, int writeFd, int playerNum);

#endif //BATTLESHIPS_USER_H
