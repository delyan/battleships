#include <stdio.h>
#include <zconf.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"
#include "user.h"

int main() {
    int parent_pipe[2], child_pipe[2];
    Board childBoard;
    Board parentBoard;
    int pid;

    for (int row = 0; row < 10; row++) {
        for (int col = 0; col < 10; col++) {
            childBoard[row][col] = 0;
            parentBoard[row][col] = 0;
        }
    }

    if(pipe(parent_pipe) || pipe(child_pipe)) {
        perror("pipe(...)");
        exit(1);
    }

    // Forking for creating a new process
    printf("Player 1 board setup:\n\n");
    deployShips(childBoard);
    displayOwnBoard(childBoard);
    printf("\n");
    printf("Player 2 board setup:\n\n");
    deployShips(parentBoard);
    displayOwnBoard(parentBoard);
    printf("\n");

    pid = fork();

    if (pid == -1) {
        perror("fork()");
        exit(1);
    }

    if (pid == 0) {
        int in = child_pipe[0];
        int out = parent_pipe[1];

        makePlayerTurn(in, out, 1);
    } else {
        int in = parent_pipe[0];
        int out = child_pipe[1];

        Board boards[2];
        memcpy(boards[0], parentBoard, sizeof(Board));
        memcpy(boards[1], childBoard, sizeof(Board));
        write(out, boards, 2 * sizeof(Board));
        makePlayerTurn(in, out, 2);
    }

    close(child_pipe[0]);
    close(child_pipe[1]);
    close(parent_pipe[0]);
    close(parent_pipe[1]);

    exit(1);
}
