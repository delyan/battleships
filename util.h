//
// Created by DelyanPeev on 6/3/18.
//

#ifndef BATTLESHIPS_UTIL_H
#define BATTLESHIPS_UTIL_H


#define BOARD_SIZE 10
#define LEFT 0
#define UP 1
#define RIGHT 2
#define DOWN 3
#define SUNK_SHIP_CHAR '-'
#define HIT_CHAR 'o'
#define EMPTY_CHAR ' '
#define HIT_EMPTY_CHAR 'x'
#define HIT_EMPTY BOARD_SIZE + 1

typedef int Board[BOARD_SIZE][BOARD_SIZE];

typedef struct Boards {
    Board ownBoard;
    Board rivalBoard;
} Boards;

void deployShips(Board* board);
void displayOwnBoard(Board* board);
void displayBoard(Board* board);
int isGameOver(Board* board);

#endif //BATTLESHIPS_UTIL_H
