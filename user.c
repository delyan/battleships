#include <zconf.h>
#include <printf.h>
#include <string.h>
#include "user.h"
#include "util.h"

void shoot(Board* rivalBoard) {
    int x, y;

    printf("\n");
    displayBoard(rivalBoard);

    printf("Enter x coordinate to shoot:");
    scanf("%d", &x);
    printf("Enter y coordinate to shoot:");
    scanf("%d", &y);

    if ((*rivalBoard)[y][x] > 0) {
        (*rivalBoard)[y][x] = -(*rivalBoard)[y][x];
        printf("You have hit a ship!");

        if (isGameOver(rivalBoard)) {
            return;
        }

        printf("Shoot again!\n");

        shoot(rivalBoard);
    } else if ((*rivalBoard)[y][x] == HIT_EMPTY || (*rivalBoard)[y][x] < 0) {
        printf("Already shot at that field. Shoot again!");
        shoot(rivalBoard);
    } else if ((*rivalBoard)[y][x] == 0) {
        (*rivalBoard)[y][x] = HIT_EMPTY;
        printf("Missed shot.");
    }
}

void makePlayerTurn(int readFd, int writeFd, int playerNum) {
    Board boards[2];

    while(1) {
        if (read(readFd, boards, 2 * sizeof(Board)) > 0) {
            Board rivalBoard;
            Board ownBoard;

            memcpy(rivalBoard, boards[0], sizeof(Board));
            memcpy(ownBoard, boards[1], sizeof(Board));

            printf("\n\n\nPlayer %d to shoot:\n", playerNum);

            shoot(rivalBoard);

            if (isGameOver(rivalBoard)) {
                printf(" Player %d has won the game!", playerNum);
                break;
            }

            printf("\nRIVAL BOARD\n");
            displayBoard(&rivalBoard);

            Board boards[2];
            memcpy(boards[0], ownBoard, sizeof(Board));
            memcpy(boards[1], rivalBoard, sizeof(Board));
            write(writeFd, boards, 2 * sizeof(Board));
        } else {
            sleep(2);
        }
    }
}
