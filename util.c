#include <printf.h>
#include <stdbool.h>

#include "util.h"

void populateValidDirections(int* validDirections, int id, int y, int x, Board* board) {
    int count;

    // Check left
    count = 0;

    for (int col = x; col >= 0; col--) {
        if ((*board)[y][col] == 0) {
            count++;
        } else break;
    }

    if (count >= id) {
        *(validDirections + LEFT) = 1;
    }

    // Check up
    count = 0;

    for (int row = y; row >= 0; row--) {
        if ((*board)[row][x] == 0) {
            count++;
        } else break;
    }

    if (count >= id) {
        *(validDirections + UP) = 1;
    }

    // Check right
    count = 0;

    for (int col = x; col < BOARD_SIZE; col++) {
        if ((*board)[y][col] == 0) {
            count++;
        } else break;
    }

    if (count >= id) {
        *(validDirections + RIGHT) = 1;
    }

    // Check down
    count = 0;

    for (int row = y; row < BOARD_SIZE; row++) {
        if ((*board)[row][x] == 0) {
            count++;
        } else break;
    }

    if (count >= id) {
        *(validDirections + DOWN) = 1;
    }
}

int isShipSunk(int id, Board* board) {
    int count = 0;

    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = 0; col < BOARD_SIZE; col++) {
            if ((*board)[row][col] == id) {
                count++;
            }
        }
    }

    return count == (id * -1) ? 1 : 0;
}

void displayOwnBoard(Board* board) {
    char c;

    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = 0; col < BOARD_SIZE; col++) {
            if ((*board)[row][col] > 0) {
                c = 'o';
            } else {
                c = ' ';
            }

            printf("[%c]", c);
        }

        printf("\n");
    }
}

char resolveDisplayChar(Board* board, int row, int col) {
    if ((*board)[row][col] < 0) {
        if (isShipSunk((*board)[row][col], board)) {
            return SUNK_SHIP_CHAR;
        }

        return HIT_CHAR;
    } else if ((*board)[row][col] == HIT_EMPTY) {
        return HIT_EMPTY_CHAR;
    }

    return EMPTY_CHAR;
}

void displayBoard(Board* board) {
    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = 0; col < BOARD_SIZE; col++) {
            printf("[%c]", resolveDisplayChar(board, row, col));
        }
        printf("\n");
    }
}

void deployShipToBoard(int shipId, int row, int col, int direction, Board* board) {
    int i;

    for (i = 0; i < shipId; i++) {
        (*board)[row][col] = shipId;

        switch(direction) {
            case LEFT: col--; break;
            case UP: row--; break;
            case RIGHT: col++; break;
            case DOWN: row++; break;
        }
    }
}

void promptShipCoordinates(int *row, int* col) {
    printf("Enter starting x coordinate for ship (0 to 9): ");
    scanf("%d", col);
    fflush(stdin);
    printf("Enter starting y coordinate for ship (0 to 9): ");
    scanf("%d", row);
    fflush(stdin);
}

int hasValidDirections(int validDirections[4]) {
    for (int i = 0; i < 4; i++) {
        if (validDirections[i]) {
            return 1;
        }
    }

    printf("No valid directions to place ship in. Try again\n");
    return 0;
}

void deployShip(int shipId, Board* board) {
    int row, col, directionChoice;
    int validDirections[4] = {0, 0, 0, 0};

    do {
        promptShipCoordinates(&row, &col);
        populateValidDirections(validDirections, shipId, row, col, board);
    } while (!hasValidDirections(validDirections));

    printf("Choose a direction from: \n");
    for (int i = 0; i < 4; i++) {
        if (*(validDirections + i)) {
            switch (i) {
                case LEFT:
                    printf("0 for LEFT\n");
                    break;
                case UP:
                    printf("1 for UP\n");
                    break;
                case RIGHT:
                    printf("2 for RIGHT\n");
                    break;
                case DOWN:
                    printf("3 for DOWN\n");
                    break;
                default: ;
            }
        }
    }

    scanf("%d", &directionChoice);
    fflush(stdin);
    deployShipToBoard(shipId, row, col, directionChoice, board);
}

void deployShips(Board* board) {
    for (int i = 1; i <= 3; i++) {
        deployShip(i, board);
    }
}

int isGameOver(Board* board) {
    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = 0; col < BOARD_SIZE; col++) {
            if ((*board)[row][col] > 0 && (*board)[row][col] != HIT_EMPTY) {
                return 0;
            }
        }
    }

    return 1;
}